package addressbook.controller;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import addressbook.model.Contact;
import addressbook.service.AddressBookService;

@Path("/addressbookcontroller")
@Produces(MediaType.APPLICATION_JSON)
public class AddressBookController {

	private static final AddressBookService SERVICE = new AddressBookService();
	
	@PermitAll
	@GET
	@Path("/list/{name}")
	public Contact listByName(@PathParam("name") String name) {
		return SERVICE.listByName(name);
	}
	
	@RolesAllowed("ADMIN")
	@POST
	@Path("/add")
	@Consumes(MediaType.APPLICATION_JSON)
	public void addContact(Contact contact) {
		SERVICE.add(contact);
	}
	
	@RolesAllowed("ADMIN")
	@PUT
	@Path("/update/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void updateContact(@PathParam("name") String name, Contact updatedContactInfo) {
		SERVICE.update(name, updatedContactInfo);
	}
	
	@RolesAllowed("ADMIN")
	@DELETE
	@Path("/delete/{name}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteContact(@PathParam("name") String name) {
		SERVICE.delete(name);
	}

}
