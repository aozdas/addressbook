package addressbook;

import org.jboss.resteasy.plugins.server.sun.http.SunHttpJaxrsServer;
import org.jboss.resteasy.spi.ResteasyDeployment;

public class AddressBook {

	public static void main(String[] args) {
		SunHttpJaxrsServer server = new SunHttpJaxrsServer();

		ResteasyDeployment deployment = new ResteasyDeployment();
		AddressBookApp application = new AddressBookApp();
		deployment.setApplication(application);

		server.setDeployment(deployment);
		server.setRootResourcePath("addressbook");

		server.setPort(8081);

		server.start();
	}
}
