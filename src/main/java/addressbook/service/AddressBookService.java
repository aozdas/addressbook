package addressbook.service;

import java.util.ArrayList;
import java.util.List;

import addressbook.model.Contact;

public class AddressBookService {

	private List<Contact> contactList = new ArrayList<Contact>();
	
	{
		contactList.add(new Contact("Ali", "Istanbul", "05398288381", "ali.ozdas@obss.com.tr"));
	}
	
	public Contact listByName(String name) {
		for (Contact contact : contactList) {
			if (contact.getName().equals(name)) {
				return contact;
			}
		}
		return null;
	}

	public void add(Contact contact) {
		contactList.add(contact);
	}

	public void update(String name, Contact updatedContactInfo) {
		for (int i = 0; i < contactList.size(); i++) {
			if (contactList.get(i).getName().equals(name)) {
				contactList.remove(i);
				contactList.add(i, updatedContactInfo);
				break;
			}
		}
	}

	public void delete(String name) {
		for (Contact contact : contactList) {
			if (contact.getName().equals(name)) {
				contactList.remove(contact);
				break;
			}
		}
	}
}
