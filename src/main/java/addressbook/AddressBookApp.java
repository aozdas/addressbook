package addressbook;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import addressbook.controller.AddressBookController;
import addressbook.security.SecurityInterceptor;

@ApplicationPath("/*")
public class AddressBookApp extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> controllers = new HashSet<>();
		controllers.add(AddressBookController.class);
		return controllers;
	}
	
	@Override
	public Set<Object> getSingletons() {
		Set<Object> instances = new HashSet<>();
		instances.add(new SecurityInterceptor());
		instances.add(new JacksonJsonProvider());
		return instances;
	}
}
